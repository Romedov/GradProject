﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kassa.Models.EventsArgs
{
    public class ReceiptPostedEventArgs : EventArgsBase
    {
        public ReceiptPostedEventArgs(string message, bool successful = true) : base(message, successful)
        {
            
        }
    }
}
